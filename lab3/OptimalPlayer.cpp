#include "OptimalPlayer.h"

OptimalPlayer::OptimalPlayer(Snake* snake, Snake* enemy, Food* food) {
	this->snake = snake;
	this->enemy = enemy;
	this->food = food;
    this->nextDirection = snake->getDirection();
}

Snake::Direction OptimalPlayer::getNextDirection() const {
    return this->nextDirection;
}

Coords OptimalPlayer::countNextSnakePosition() {
    Snake::Direction snakeDirection = this->snake->getDirection();
    Coords snakePosition = this->snake->getPosition();

    if (snakeDirection == Snake::Direction::LEFT) {
        return snakePosition + Coords(-config::cellSize, 0);
    }
    else if (snakeDirection == Snake::Direction::RIGHT) {
        return snakePosition + Coords(config::cellSize, 0);
    }
    else if (snakeDirection == Snake::Direction::UP) {
        return snakePosition + Coords(0, -config::cellSize);
    }
    else if (snakeDirection == Snake::Direction::DOWN) {
        return snakePosition + Coords(0, config::cellSize);
    }
}

void OptimalPlayer::nextStep() {
    Coords snakePosition = this->countNextSnakePosition();
    int snakePositionCoordX = snakePosition.getCoordX();
    int snakePositionCoordY = snakePosition.getCoordY();

    int foodPositionCoordX = this->food->getPosition().getCoordX();
    int foodPositionCoordY = this->food->getPosition().getCoordY();
    
    if (this->snake->getDirection() == Snake::Direction::LEFT || this->snake->getDirection() == Snake::Direction::RIGHT) {
       if (snakePositionCoordX == foodPositionCoordX || !this->isLeftFree() || !this->isRightFree()) {
            if (snakePositionCoordY < foodPositionCoordY) {
                this->nextDirection = Snake::Direction::DOWN;
            }
            else {
                this->nextDirection = Snake::Direction::UP;
            }
        }
        if (!this->isDownFree()) {
            this->nextDirection = Snake::Direction::UP;
        }
        if (!this->isUpFree()) {
            this->nextDirection = Snake::Direction::DOWN;
        }
    }
    else if (this->snake->getDirection() == Snake::Direction::UP || this->snake->getDirection() == Snake::Direction::DOWN) {
        if (snakePositionCoordY == foodPositionCoordY || !this->isUpFree() || !this->isDownFree()) {
            if (snakePositionCoordX < foodPositionCoordX) {
                this->nextDirection = Snake::Direction::RIGHT;
            }
            else {
                this->nextDirection = Snake::Direction::LEFT;
            }
        }
        if (!this->isRightFree()) {
            this->nextDirection = Snake::Direction::LEFT;
        }
        if (!this->isLeftFree()) {
            this->nextDirection = Snake::Direction::RIGHT;
        }
    }
}

bool OptimalPlayer::isLeftFree() {
    Coords snakePosition = this->countNextSnakePosition();
    int snakePositionCoordX = snakePosition.getCoordX();
    int snakePositionCoordY = snakePosition.getCoordY();
    
    Coords* enemyBody = this->enemy->getBody();
    bool res = true;
    for (size_t i = 0; i < this->enemy->getLength(); i++) {
        if (snakePositionCoordY == enemyBody[i].getCoordY()) {
            res = (snakePositionCoordX != (enemyBody[i].getCoordX() + config::cellSize));
            res &= !(snakePositionCoordX == 0 && enemyBody[i].getCoordX() == (config::windowWidth - config::cellSize));
        }
    }

    if (this->snake->getDirection() != Snake::Direction::RIGHT) {
        Coords* snakeBody = this->snake->getBody();
        for (size_t i = 0; i < this->snake->getLength(); i++) {
            if (snakePositionCoordY == snakeBody[i].getCoordY()) {
                res = (snakePositionCoordX != (snakeBody[i].getCoordX() + config::cellSize));
                res &= !(snakePositionCoordX == 0 && snakeBody[i].getCoordX() == (config::windowWidth - config::cellSize));
            }
        }
    }
    
    return res;
}

bool OptimalPlayer::isRightFree() {
    Coords snakePosition = this->countNextSnakePosition();
    int snakePositionCoordX = snakePosition.getCoordX();
    int snakePositionCoordY = snakePosition.getCoordY();

    Coords* enemyBody = this->enemy->getBody();
    bool res = true;
    for (size_t i = 0; i < this->enemy->getLength(); i++) {
        if (snakePositionCoordY == enemyBody[i].getCoordY()) {
            res = (snakePositionCoordX != (enemyBody[i].getCoordX() - config::cellSize));
            res &= !(snakePositionCoordX == (config::windowWidth - config::cellSize) && enemyBody[i].getCoordX() == 0);
        }
    }

    if (this->snake->getDirection() != Snake::Direction::LEFT) {
        Coords* snakeBody = this->snake->getBody();
        for (size_t i = 0; i < this->snake->getLength(); i++) {
            if (snakePositionCoordY == snakeBody[i].getCoordY()) {
                res = (snakePositionCoordX != (snakeBody[i].getCoordX() - config::cellSize));
                res &= !(snakePositionCoordX == (config::windowWidth - config::cellSize) && snakeBody[i].getCoordX() == 0);
            }
        }
    }

    return res;
}

bool OptimalPlayer::isUpFree() {
    Coords snakePosition = this->countNextSnakePosition();
    int snakePositionCoordX = snakePosition.getCoordX();
    int snakePositionCoordY = snakePosition.getCoordY();

    Coords* enemyBody = this->enemy->getBody();
    bool res = true;
    for (size_t i = 0; i < this->enemy->getLength(); i++) {
        if (snakePositionCoordX == enemyBody[i].getCoordX()) {
            res = (snakePositionCoordY != (enemyBody[i].getCoordY() + config::cellSize));
            res &= !(snakePositionCoordY == 0 && enemyBody[i].getCoordY() == (config::windowHeight - config::cellSize));
        }
    }

    if (this->snake->getDirection() != Snake::Direction::DOWN) {
        Coords* snakeBody = this->snake->getBody();
        for (size_t i = 0; i < this->snake->getLength(); i++) {
            if (snakePositionCoordX == snakeBody[i].getCoordX()) {
                res = (snakePositionCoordY != (snakeBody[i].getCoordY() + config::cellSize));
                res &= !(snakePositionCoordY == 0 && snakeBody[i].getCoordY() == (config::windowHeight - config::cellSize));
            }
        }
    }

    return res;
}

bool OptimalPlayer::isDownFree() {
    Coords snakePosition = this->countNextSnakePosition();
    int snakePositionCoordX = snakePosition.getCoordX();
    int snakePositionCoordY = snakePosition.getCoordY();

    Coords* enemyBody = this->enemy->getBody();
    bool res = true;
    for (size_t i = 0; i < this->enemy->getLength(); i++) {
        if (snakePositionCoordX == enemyBody[i].getCoordX()) {
            res = (snakePositionCoordY != (enemyBody[i].getCoordY() - config::cellSize));
            res &= !(snakePositionCoordY == (config::windowHeight - config::cellSize) && enemyBody[i].getCoordY() == 0);
        }
    }

    if (this->snake->getDirection() != Snake::Direction::UP) {
        Coords* snakeBody = this->snake->getBody();
        for (size_t i = 0; i < this->snake->getLength(); i++) {
            if (snakePositionCoordX == snakeBody[i].getCoordX()) {
                res = (snakePositionCoordY != (snakeBody[i].getCoordY() - config::cellSize));
                res &= !(snakePositionCoordY == (config::windowHeight - config::cellSize) && snakeBody[i].getCoordY() == 0);
            }
        }
    }
    
    return res;
}