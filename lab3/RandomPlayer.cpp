#include "RandomPlayer.h"
#include <iostream>

RandomPlayer::RandomPlayer(Snake* snake) {
	this->snake = snake;
	this->nextDirection = snake->getDirection();
}

Snake::Direction RandomPlayer::getNextDirection() const {
	return this->nextDirection;
}

void RandomPlayer::nextStep() {
	std::srand(std::time(nullptr));
	int direction = std::rand() % 4;
	if (direction == 0) this->nextDirection = Snake::Direction::LEFT;
	else if (direction == 1) this->nextDirection = Snake::Direction::RIGHT;
	else if (direction == 2) this->nextDirection = Snake::Direction::UP;
	else if (direction == 3) this->nextDirection = Snake::Direction::DOWN;

	if (this->nextDirection == Snake::Direction::LEFT)
		std::cout << "left" <<std::endl;
	if (this->nextDirection == Snake::Direction::RIGHT)
		std::cout << "right" << std::endl;
	if (this->nextDirection == Snake::Direction::UP)
		std::cout << "up" << std::endl;
	if (this->nextDirection == Snake::Direction::DOWN)
		std::cout << "down" << std::endl;
}