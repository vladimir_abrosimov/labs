#include "GameCore.h"

bool GameCore::run() {
    Food food;

    Snake snake1(&food, Coords(config::windowWidth - config::cellSize, config::windowHeight - config::cellSize));
    Snake snake2(&food, Coords(0,0));

    std::shared_ptr<Player>player1;
    std::shared_ptr<Player>player2;

    GameView view;

    Control characterSelectionMenuComand = view.displayPlayerSelectionMenu(1);
    if (characterSelectionMenuComand == Control::SELECT_REAL_PLAYER) {
        player1 = std::make_shared<RealPlayer>(&snake1, RealPlayer::ControlScheme::ARROW_KEYS);
    }
    else if (characterSelectionMenuComand == Control::SELECT_OPTIMAL_PLAYER) {
        player1 = std::make_shared<OptimalPlayer>(&snake1, &snake2, &food);
    }
    else if (characterSelectionMenuComand == Control::SELECT_RANDOM_PLAYER) {
        player1 = std::make_shared<RandomPlayer>(&snake1);
    }
    else if (characterSelectionMenuComand == Control::EXIT_GAME) {
        return false;
    }

    characterSelectionMenuComand = view.displayPlayerSelectionMenu(2);
    if (characterSelectionMenuComand == Control::SELECT_REAL_PLAYER) {
        player2 = std::make_shared<RealPlayer>(&snake2, RealPlayer::ControlScheme::WASD);
    }
    else if (characterSelectionMenuComand == Control::SELECT_OPTIMAL_PLAYER) {
        player2 = std::make_shared<OptimalPlayer>(&snake2, &snake1, &food);
    }
    else if (characterSelectionMenuComand == Control::SELECT_RANDOM_PLAYER) {
        player2 = std::make_shared<RandomPlayer>(&snake2);
    }
    else if (characterSelectionMenuComand == Control::EXIT_GAME) {
        return false;
    }

    while (true) {
        Control showGameComand = view.displayGame(player1, player2, &snake1, &snake2, &food);
        if (showGameComand == Control::RELOAD_GAME) {
            return true;
        }
        else if (showGameComand == Control::EXIT_GAME) {
            return false;
        }

        Control pauseMenuComand = view.displayPauseMenu();
        if (pauseMenuComand == Control::EXIT_GAME) {
            return false;
        }
    }
}
