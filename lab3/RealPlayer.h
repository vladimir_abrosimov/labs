#pragma once

#include <SFML/Graphics.hpp>
#include "Player.h"
#include "Snake.h"

class RealPlayer : public Player{
public:
	enum class ControlScheme {
		WASD,
		ARROW_KEYS
	};
	RealPlayer(Snake* snake, ControlScheme controlScheme = ControlScheme::ARROW_KEYS);
	~RealPlayer() = default;
	void nextStep() override;
	Snake::Direction getNextDirection() const override;

private:
	ControlScheme controlScheme;
	Snake::Direction nextDirection;
};

