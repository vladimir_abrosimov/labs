#include "RealPlayer.h"

RealPlayer::RealPlayer(Snake* snake, ControlScheme controlScheme) {
   	this->snake = snake;
    this->controlScheme = controlScheme;
    this->nextDirection = snake->getDirection();
}

Snake::Direction RealPlayer::getNextDirection() const {
    return this->nextDirection;
}

void RealPlayer::nextStep() {
    if (controlScheme == RealPlayer::ControlScheme::ARROW_KEYS) {
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
            //this->snake->turn(Snake::Direction::LEFT);
            this->nextDirection = Snake::Direction::LEFT;
        }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
            //this->snake->turn(Snake::Direction::RIGHT);
            this->nextDirection = Snake::Direction::RIGHT;
        }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
            //this->snake->turn(Snake::Direction::UP);
            this->nextDirection = Snake::Direction::UP;
        }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
            //this->snake->turn(Snake::Direction::DOWN);
            this->nextDirection = Snake::Direction::DOWN;
        }
    }
    else if (controlScheme == RealPlayer::ControlScheme::WASD) {
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
            //this->snake->turn(Snake::Direction::LEFT);
            this->nextDirection = Snake::Direction::LEFT;
        }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
            //this->snake->turn(Snake::Direction::RIGHT);
            this->nextDirection = Snake::Direction::RIGHT;
        }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
            //this->snake->turn(Snake::Direction::UP);
            this->nextDirection = Snake::Direction::UP;
        }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
            //this->snake->turn(Snake::Direction::DOWN);
            this->nextDirection = Snake::Direction::DOWN;
        }
    }
    
}