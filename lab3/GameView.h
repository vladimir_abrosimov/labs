#pragma once

#include <SFML/Graphics.hpp>
#include "Config.h"
#include "Snake.h"
#include "Coords.h"
#include "Player.h"
#include "RealPlayer.h"
#include "OptimalPlayer.h"

enum class Control {
	START_GAME,
	EXIT_GAME,
	PAUSE_GAME,
	RELOAD_GAME,
	CONTINUE_GAME,
	SELECT_REAL_PLAYER,
	SELECT_OPTIMAL_PLAYER,
	SELECT_RANDOM_PLAYER
};

class GameView {
public:
	GameView();
	Control displayPauseMenu() const;
	Control displayPlayerSelectionMenu(unsigned int playerNum) const;
	void displayMessage(std::string message) const;
	Control displayGame(std::shared_ptr<Player> player1, std::shared_ptr<Player> player2, Snake* snake1, Snake* snake2, Food* food);

private:
	std::shared_ptr<sf::RenderWindow> window;
	sf::Font font;

	void showWinner(std::shared_ptr<Player> player1, std::shared_ptr<Player> player2);
	void viewSnake(Snake snake, sf::RectangleShape snakeCell);
	void viewFood(Food food, sf::RectangleShape foodCell);
	bool nextStep(std::shared_ptr<Player> player1, std::shared_ptr<Player> player2, Snake* snake1, Snake* snake2, double timer, sf::Text* scoreText1, sf::Text* scoreText2);
	Control gameHandleEvent();
	bool viewGameWindow(std::shared_ptr<Player> player1, std::shared_ptr<Player> player2, Snake* snake1, Snake* snake2, Food* food, sf::RectangleShape snakeCell, sf::RectangleShape foodCell, sf::Text scoreText1, sf::Text scoreText2);
};

