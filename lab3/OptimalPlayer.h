#pragma once

#include <SFML/Graphics.hpp>
#include <iostream>
#include "Player.h"
#include "Snake.h"
#include "Food.h"

class OptimalPlayer : public Player {
public:
	OptimalPlayer(Snake* snake, Snake* enemy, Food* food);
	~OptimalPlayer() = default;

	void nextStep() override;
	Snake::Direction getNextDirection() const override;
	
private:
	Snake* enemy;
	Food* food;
	Snake::Direction nextDirection;

	Coords countNextSnakePosition();
	bool isLeftFree();
	bool isRightFree();
	bool isUpFree();
	bool isDownFree();
};

