#include "GameView.h"

GameView::GameView() {
    this->window = std::make_shared<sf::RenderWindow>(sf::VideoMode(config::windowWidth, config::windowHeight), "Snake");
    this->font.loadFromFile("fonts/OpenSans-Regular.ttf");
}

Control GameView::displayPauseMenu() const {
    sf::Text exitText;
    exitText.setFont(this->font);
    exitText.setString("Press ESCAPE to exit");
    exitText.setPosition(100, 200);
    exitText.setCharacterSize(36);

    sf::Text enterText;
    enterText.setFont(this->font);
    enterText.setString("Press ENTER to start");
    enterText.setPosition(100, 200 + 60);
    enterText.setCharacterSize(36);

    sf::Event event;
    while (this->window->isOpen()) {
        while (this->window->pollEvent(event)) {
            if (event.type == sf::Event::Closed) {
                return Control::EXIT_GAME;
            }
            else if (event.type == sf::Event::KeyPressed) {
                if (event.key.code == sf::Keyboard::Enter) {
                    return Control::START_GAME;
                }
                else if (event.key.code == sf::Keyboard::Escape) {
                    return Control::EXIT_GAME;
                }
            }
        }
        
        this->window->clear(config::menuWidowBgColor);
        this->window->draw(exitText);
        this->window->draw(enterText);
        this->window->display();
    }
}

Control GameView::displayPlayerSelectionMenu(unsigned int playerNum) const {
    sf::Text playerSelectText;
    playerSelectText.setFont(this->font);
    playerSelectText.setString("Press button to select player " + std::to_string(playerNum) + " type:");
    playerSelectText.setPosition(100, 200);
    playerSelectText.setCharacterSize(36);

    sf::Text realPlayerText;
    realPlayerText.setFont(this->font);
    realPlayerText.setString("1 - Real player");
    realPlayerText.setPosition(100, 200 + 60);
    realPlayerText.setCharacterSize(28);

    sf::Text optimalPlayerText;
    optimalPlayerText.setFont(this->font);
    optimalPlayerText.setString("2 - Optimal player");
    optimalPlayerText.setPosition(100, 200 + 100);
    optimalPlayerText.setCharacterSize(28);

    sf::Text randomPlayerText;
    randomPlayerText.setFont(this->font);
    randomPlayerText.setString("3 - Random player");
    randomPlayerText.setPosition(100, 200 + 140);
    randomPlayerText.setCharacterSize(28);

    sf::Event event;
    while (this->window->isOpen()) {
        while (this->window->pollEvent(event)) {
            if (event.type == sf::Event::Closed) {
                return Control::EXIT_GAME;
            }
            else if (event.type == sf::Event::KeyPressed) {
                if (event.key.code == sf::Keyboard::Num1) {
                    return Control::SELECT_REAL_PLAYER;
                }
                else if (event.key.code == sf::Keyboard::Num2) {
                    return Control::SELECT_OPTIMAL_PLAYER;
                }
                else if (event.key.code == sf::Keyboard::Num3) {
                    return Control::SELECT_RANDOM_PLAYER;
                }
                else if (event.key.code == sf::Keyboard::Escape) {
                    return Control::EXIT_GAME;
                }
            }
        }
        
        this->window->clear(config::menuWidowBgColor);
        this->window->draw(playerSelectText);
        this->window->draw(realPlayerText);
        this->window->draw(optimalPlayerText);
        this->window->draw(randomPlayerText);
        this->window->display();
    }
}

void GameView::displayMessage(std::string message) const {
    sf::Text winText;
    winText.setFont(this->font);
    winText.setString(message);
    winText.setPosition(260, 200);
    winText.setCharacterSize(42);

    sf::Clock clock;
    double timer = 0;
    double textDisplayTime = 2;

    sf::Event event;
    while (this->window->isOpen()) {
        float time = clock.getElapsedTime().asSeconds();
        clock.restart();
        timer += time;

        while (this->window->pollEvent(event)) {
            if (event.type == sf::Event::Closed) {
                this->window->close();
            }
        }

        if (timer > textDisplayTime) {
            return;
        }

        this->window->clear(config::menuWidowBgColor);
        this->window->draw(winText);
        this->window->display();
    }
}


Control GameView::displayGame(std::shared_ptr<Player> player1, std::shared_ptr<Player> player2, Snake* snake1, Snake* snake2, Food* food) {
    sf::RectangleShape snakeCell(sf::Vector2f(config::cellSize, config::cellSize));
    snakeCell.setFillColor(config::snakeColor);

    sf::RectangleShape foodCell(sf::Vector2f(config::cellSize, config::cellSize));
    foodCell.setFillColor(config::foodColor);

    sf::Text scoreText1;
    scoreText1.setFont(this->font);
    scoreText1.setPosition(20, 20);
    scoreText1.setCharacterSize(22);

    sf::Text scoreText2;
    scoreText2.setFont(this->font);
    scoreText2.setPosition(config::windowWidth - 120, 20);
    scoreText2.setCharacterSize(22);

    sf::Clock clock;
    double timer = 0;

    while (this->window->isOpen()) {
        float time = clock.getElapsedTime().asSeconds();
        clock.restart();
        timer += time;

        Control eventComand = this->gameHandleEvent();
        if (eventComand != Control::CONTINUE_GAME) {
            return eventComand;
        }
        
        if (this->nextStep(player1, player2, snake1, snake2, timer, &scoreText1, &scoreText2)) {
            timer = 0;
        }

        if (!this->viewGameWindow(
            player1, player2, snake1, snake2, food, snakeCell, foodCell, scoreText1, scoreText2
        )) return Control::RELOAD_GAME;
    }
}

Control GameView::gameHandleEvent() {
    sf::Event event;
    while (this->window->pollEvent(event)) {
        if (event.type == sf::Event::Closed) {
            return Control::EXIT_GAME;
        }
        else if (event.type == sf::Event::KeyPressed) {
            if (event.key.code == sf::Keyboard::Escape) {
                return Control::PAUSE_GAME;
            }
        }
        else {
            return Control::CONTINUE_GAME;
        }
    }
    return Control::CONTINUE_GAME;
}

bool GameView::nextStep(std::shared_ptr<Player> player1, std::shared_ptr<Player> player2, Snake* snake1, Snake* snake2, double timer, sf::Text* scoreText1, sf::Text* scoreText2) {
    if (snake1->isAlive()) player1->nextStep();
    if (snake2->isAlive()) player2->nextStep();

    if (timer > config::snakeVelocity) {
        if (snake1->isAlive()) snake1->nextStep(*snake2);
        if (snake2->isAlive()) snake2->nextStep(*snake1);
        
        snake1->turn(player1->getNextDirection());
        snake2->turn(player2->getNextDirection());

        scoreText1->setString("Score: " + std::to_string(player1->getScore()));
        scoreText2->setString("Score: " + std::to_string(player2->getScore()));
        
        return true;
    }
    return false;
}

bool GameView::viewGameWindow(std::shared_ptr<Player> player1, std::shared_ptr<Player> player2, Snake* snake1, Snake* snake2, Food* food, sf::RectangleShape snakeCell, sf::RectangleShape foodCell, sf::Text scoreText1, sf::Text scoreText2) {
    this->window->clear(config::gameWidowBgColor);
    this->window->draw(scoreText1);
    this->window->draw(scoreText2);

    if (snake1->isAlive()) {
        this->viewSnake(*(snake1), snakeCell);
    }
    if (snake2->isAlive()) {
        this->viewSnake(*(snake2), snakeCell);
    }
    if (!snake1->isAlive() && !snake2->isAlive()) {
        this->showWinner(player1, player2);
        return false;
    }

    this->viewFood(*(food), foodCell);
    this->window->display();

    return true;
}

void GameView::showWinner(std::shared_ptr<Player> player1, std::shared_ptr<Player> player2) {
    if (player1->getScore() > player2->getScore()) {
        this->displayMessage("Player 1 wins!");
    }
    else if (player2->getScore() > player1->getScore()) {
        this->displayMessage("Player 2 wins!");
    }
    else {
        this->displayMessage("Draw!");
    }
}

void GameView::viewSnake(Snake snake, sf::RectangleShape snakeCell) {
    for (int i = 0; i < snake.getLength(); i++) {
        Coords c = snake.getBody()[i];
        snakeCell.setPosition(c.getCoordX(), c.getCoordY());
        this->window->draw(snakeCell);
    }
}

void GameView::viewFood(Food food, sf::RectangleShape foodCell) {
    Coords foodPosition = food.getPosition();
    foodCell.setPosition(foodPosition.getCoordX(), foodPosition.getCoordY());
    this->window->draw(foodCell);
}