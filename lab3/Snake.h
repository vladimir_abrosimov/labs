#pragma once

#include <vector>
#include "Config.h"
#include "Coords.h"
#include "Food.h"

class Snake {
public:
    enum class Direction {
        LEFT,
        RIGHT,
        UP,
        DOWN
    };

    Snake(Food* food);
    Snake(Food* food, Coords position) : Snake(food) { this->body[0] = position; };
    Snake(const Snake& other);
    ~Snake();
    Coords getPosition() const;
    unsigned int getLength() const;
    unsigned int getCountEatenFood() const;
    Direction getDirection() const;
    Coords* getBody() const;
    
    void turn(Direction direction);
    void nextStep(const Snake& other);
    bool isAlive() const;
    
private:
    Food* food;
    Coords* body;
    size_t bodySize;
    Direction direction;
    unsigned int length;
    bool alive;
    unsigned int countEatenFood;

    void move();
    void checkOutBorder();
    void checkFood();
    bool isCrashed() const;
    bool isCrashed(const Snake& other) const;
    void grow();
    void kill();
};