#pragma once

#include "Snake.h"

class Player {
public:
	virtual ~Player() {};
	virtual void nextStep() = 0;
	unsigned int getScore() const;

	virtual Snake::Direction getNextDirection() const = 0;

protected:
	Snake* snake;
	unsigned int score;
};