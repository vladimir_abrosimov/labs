#include "Food.h"

Food::Food() {
	this->generateFood();
}

Coords Food::getPosition() const {
	return this->position;
}

void Food::generateFood() {
	int x = (std::rand() % (config::windowWidth / config::cellSize)) * config::cellSize;
	int y = (std::rand() % (config::windowHeight / config::cellSize)) * config::cellSize;
	Coords foodPosition = Coords(x, y);

	if (foodPosition == this->getPosition()) this->generateFood();
	this->position = foodPosition;
}