#pragma once

#include <ctime>
#include "Player.h"

class RandomPlayer : public Player {
public:
	RandomPlayer(Snake* snake);
	~RandomPlayer() = default;
	void nextStep() override;
	Snake::Direction getNextDirection() const override;

private:
	Snake::Direction nextDirection;
};

