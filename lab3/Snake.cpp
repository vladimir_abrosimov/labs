#include "Snake.h"

Snake::Snake(Food* food) {
    this->food = food;
    this->bodySize = 100;
    this->body = new Coords[this->bodySize];
    this->direction = Direction::RIGHT;
    this->length = 1;
    this->alive = true;
    this->countEatenFood = 0;
}

Snake::Snake(const Snake& other) {
    this->food = other.food;
    this->bodySize = other.bodySize;
    this->body = other.body;
    this->direction = other.direction;
    this->length = other.length;
    this->alive = other.alive;
    this->countEatenFood = other.countEatenFood;

    this->body = new Coords[other.bodySize];
    for (int i = 0; i < other.length; i++) {
        this->body[i] = other.body[i];
    }
}

Snake::~Snake() { 
    delete[] this->body; 
}

Coords Snake::getPosition() const {
    return this->body[0];
}

unsigned int Snake::getLength() const {
    return this->length;
}

unsigned int Snake::getCountEatenFood() const {
    return this->countEatenFood;
}

Snake::Direction Snake::getDirection() const {
    return this->direction;
}

Coords* Snake::getBody() const {
    return this->body;
}

void Snake::turn(Snake::Direction direction) {
    if (this->direction == Snake::Direction::LEFT && direction == Snake::Direction::RIGHT) {
        return;
    } else if (this->direction == Snake::Direction::RIGHT && direction == Snake::Direction::LEFT) {
        return;
    } else if (this->direction == Snake::Direction::UP && direction == Snake::Direction::DOWN) {
        return;
    } else if (this->direction == Snake::Direction::DOWN && direction == Snake::Direction::UP) {
        return;
    }
    this->direction = direction;
}

void Snake::nextStep(const Snake& other) {
    this->move();
    this->checkFood();

    if (isCrashed(other)) {
        this->kill();
    }
}

bool Snake::isAlive() const {
    return this->alive;
}

void Snake::move() {
    for (int i = this->length-1; i > 0; i--) {
        this->body[i] = this->body[i - 1];
    }
    Coords coords;
    if (this->direction == Snake::Direction::LEFT) {
        coords.setCoords(-config::cellSize, 0);
    }
    else if (this->direction == Snake::Direction::RIGHT) {
        coords.setCoords(config::cellSize, 0);
    }
    else if (this->direction == Snake::Direction::UP) {
        coords.setCoords(0, -config::cellSize);
    }
    else if (this->direction == Snake::Direction::DOWN) {
        coords.setCoords(0, config::cellSize);
    }
    this->body[0] += coords;
    this->checkOutBorder();
}

bool Snake::isCrashed() const {
    for (size_t i = 1; i < this->length; i++) {
        if (this->body[i] == this->getPosition()) {
            return true;
        }
    }
    return false;
}

bool Snake::isCrashed(const Snake& other) const {
    for (size_t i = 0; i < other.length; i++) {
        if (other.body[i] == this->getPosition()) {
            return true;
        }
    }
    return isCrashed();
}

void Snake::checkOutBorder() {
    int positionCoordX = this->getPosition().getCoordX();
    int positionCoordY = this->getPosition().getCoordY();
    
    if (positionCoordX >= config::windowWidth) {
        this->body[0] = Coords(0, positionCoordY);
    }
    else if (positionCoordX < 0) {
        this->body[0] = Coords(config::windowWidth, positionCoordY);
    }
    else if (positionCoordY >= config::windowHeight) {
        this->body[0] = Coords(positionCoordX, 0);
    }
    else if (positionCoordY < 0) {
        this->body[0] = Coords(positionCoordX, config::windowHeight);
    }
}

void Snake::checkFood() {
    if (this->getPosition() == this->food->getPosition()) {
        this->grow();
        this->countEatenFood += 1;
        this->food->generateFood();
    }
}

void Snake::grow() {
    this->length += 1;

    Coords coords;
    if (this->direction == Snake::Direction::LEFT) {
        coords.setCoords(config::cellSize, 0);
    } else if (this->direction == Snake::Direction::RIGHT) {
        coords.setCoords(-config::cellSize, 0);
    } else if (this->direction == Snake::Direction::UP) {
        coords.setCoords(0, config::cellSize);
    } else if (this->direction == Snake::Direction::DOWN) {
        coords.setCoords(0, -config::cellSize);
    }

    if (this->length > this->bodySize) {
        this->bodySize *= 2;
        Coords* newBody = new Coords[this->bodySize];

        for (size_t i = 0; i < this->length-1; i++) {
            newBody[i] = this->body[i];
        }
        delete[] this->body;
        this->body = newBody;
    }

    this->body[this->length - 1] = this->body[this->length - 2] + coords;
}

void Snake::kill() {
    this->length = 0;
    this->body[0] = Coords(-1, -1);
    this->alive = false;
}