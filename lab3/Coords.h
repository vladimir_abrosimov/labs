#pragma once

#include <utility>

class Coords {
public:
    Coords(int x, int y);
    Coords();
    void setCoords(int x, int y);
    int getCoordX() const;
    int getCoordY() const;
    bool operator== (const Coords& other);
    Coords operator+ (const Coords& other);
    Coords& operator= (const Coords& other);
    Coords& operator+= (const Coords& other);

private:
    int x;
    int y;
};