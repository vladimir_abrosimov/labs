#pragma once

#include <SFML/Graphics.hpp>
#include <string>
#include <functional>
#include "Config.h"
#include "Snake.h"
#include "Coords.h"
#include "Food.h"
#include "GameView.h"

#include "Player.h"
#include "RealPlayer.h"
#include "RandomPlayer.h"
#include "OptimalPlayer.h"

class GameCore {
public:
	static bool run();
};

