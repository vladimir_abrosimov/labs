#include <iostream>
#include <SFML/Graphics.hpp>
#include <time.h>
#include "Config.h"
#include "GameCore.h"
#include "GameView.h"
#include "Coords.h"
#include "Snake.h"
#include "Food.h"

#include "Player.h"
#include "RealPlayer.h"
#include "RandomPlayer.h"
#include "OptimalPlayer.h"

int main() {
    while (GameCore::run()) {
        continue;
    }
}