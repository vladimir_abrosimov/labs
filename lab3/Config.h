#pragma once

#include <SFML/Graphics.hpp>

namespace config {
	static int windowWidth = 800;
	static int windowHeight = 800;
	static sf::Color gameWidowBgColor(154, 204, 153);
	static sf::Color menuWidowBgColor(23, 24, 45);
	static int cellSize = 16; // windowWidth % cellSize == 0
	static sf::Color snakeColor(23, 24, 45);
	static sf::Color foodColor(30, 60, 120);
	static double snakeVelocity = 0.05;
}