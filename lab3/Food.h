#pragma once

#include <ctime>
#include <iostream>
#include "Config.h"
#include "Coords.h"

class Food {
public:
	Food();
	Coords getPosition() const;

	void generateFood();

private:
	Coords position;
};

