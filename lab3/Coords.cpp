#include "Coords.h"

Coords::Coords(int x, int y) {
	this->x = x;
	this->y = y;
}

Coords::Coords() {
	this->x = 0;
	this->y = 0;
}

void Coords::setCoords(int x, int y) {
	this->x = x;
	this->y = y;
}

int Coords::getCoordX() const {
	return this->x;
}

int Coords::getCoordY() const {
	return this->y;
}

bool Coords::operator== (const Coords& other) {
	if (this->x == other.x && this->y == other.y) {
		return true;
	} else {
		return false;
	}
}

Coords Coords::operator+ (const Coords& other) {
	return Coords(this->x+other.x, this->y+other.y);
}

Coords& Coords::operator= (const Coords& other) {
	this->x = other.x;
	this->y = other.y;
	return *this;
}

Coords& Coords::operator+= (const Coords& other) {
	*this = *this + other;
	return *this;
}